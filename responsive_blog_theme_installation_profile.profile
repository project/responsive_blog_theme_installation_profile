<?php

/**
 * @file
 * File with installation configuration.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Allows the profile to alter the site configuration form.
 */
function system_form_install_configure_form_alter(&$form, $form_state) {
  $form['site_information']['site_name']['#default_value'] = 'Responsive blog';
  $form['site_information']['site_slogan'] = array(
    '#title' => st('Site slogan'),
    '#type' => 'textfield',
    '#description' => st('Text that will be displayed next to site logo.'),
  );
  $form['actions']['submit']['#submit'][] = 'install_configure_form_submit';
  $form['actions']['submit']['#submit'][] = 'responsive_blog_theme_installation_profile_configure_form_submit_alter';
}

/**
 * Alter default submit for configuration form.
 */
function responsive_blog_theme_installation_profile_configure_form_submit_alter($form) {
  if (!empty($form['site_information']['site_slogan']['#value'])) {
    $slogan = $form['site_information']['site_slogan']['#value'];
    variable_set('site_slogan', $slogan);
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Select the current install profile by default.
 */
function system_form_install_select_profile_form_alter(&$form, $form_state) {
  foreach ($form['profile'] as $profile_name => $profile_data) {
    $form['profile'][$profile_name]['#value'] = 'responsive_blog_theme_installation_profile';
  }
}

/**
 * Implements hoot_install_tasks().
 */
function responsive_blog_theme_installation_profile_install_tasks(&$install_state) {
  $tasks['example_posts'] = array(
    'display_name' => st('Example nodes'),
    'display' => TRUE,
    'type' => 'form',
    'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
    'function' => 'responsive_blog_theme_installation_profile_example_nodes_form',
  );

  $do_task = variable_get('responsive_blog_theme_installation_profile_example_nodes', FALSE);
  $tasks['example_posts_batch'] = array(
    'type' => 'batch',
    'run' => $do_task ? INSTALL_TASK_RUN_IF_NOT_COMPLETED : INSTALL_TASK_SKIP,
    'function' => 'responsive_blog_theme_installation_profile_create_example_nodes',
  );

  return $tasks;
}

/**
 * Consctruct a form for example_posts installation task.
 */
function responsive_blog_theme_installation_profile_example_nodes_form() {
  $form['responsive_blog']['example_nodes'] = array(
    '#title' => st('Create example blog posts and slides'),
    '#type' => 'checkbox',
    '#default_value' => FALSE,
  );
  $form['responsive_blog']['submit'] = array(
    '#type' => 'submit',
    '#value' => st('Save and continue'),
    '#submit' => array('responsive_blog_theme_installation_profile_example_nodes_form_submit'),
    '#weight' => 15,
  );
  return $form;
}

/**
 * Callback for submitting 'example nodes' form.
 */
function responsive_blog_theme_installation_profile_example_nodes_form_submit($form) {
  variable_set('responsive_blog_theme_installation_profile_example_nodes', $form['responsive_blog']['example_nodes']['#value']);
}

/**
 * Callback for adding example nodes.
 */
function responsive_blog_theme_installation_profile_create_example_nodes() {
  $modules = array(
    'feature_example_nodes',
  );
  module_enable($modules, TRUE);

  variable_del('responsive_blog_theme_installation_profile_example_nodes');
}
