; responsive_blog make file for d.o. usage
core = "7.x"
api = "2"

; +++++ Modules +++++

projects[admin_menu][version] = "3.0-rc6"
projects[admin_menu][subdir] = "contrib"

projects[ctools][version] = "1.15"
projects[ctools][subdir] = "contrib"

projects[flexslider][version] = "2.0-rc2"
projects[flexslider][subdir] = "contrib"

projects[libraries][version] = "2.5"
projects[libraries][subdir] = "contrib"

projects[social_media_links][version] = "1.5"
projects[social_media_links][subdir] = "contrib"

projects[views][version] = "3.23"
projects[views][subdir] = "contrib"

projects[ckeditor][version] = "1.18"
projects[ckeditor][subdir] = "contrib"

projects[token][version] = "1.7"
projects[token][subdir] = "contrib"

projects[pathauto][version] = "1.3"
projects[pathauto][subdir] = "contrib"

projects[features][version] = "2.11"
projects[features][subdir] = "contrib"

projects[strongarm][version] = "2.0"
projects[strongarm][subdir] = "contrib"

projects[uuid][version] = "1.3"
projects[uuid][subdir] = "contrib"

projects[node_export][version] = "3.1"
projects[node_export][subdir] = "contrib"

; +++++ Themes +++++

; adaptivetheme
projects[adaptivetheme][type] = "theme"
projects[adaptivetheme][version] = "3.4"
projects[adaptivetheme][subdir] = "contrib"

; adaptivetheme
projects[responsive_blog_theme][type] = "theme"
projects[responsive_blog_theme][version] = "1.1"
projects[responsive_blog_theme][subdir] = "contrib"
